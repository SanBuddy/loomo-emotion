package com.example.android.camera2video;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.camera2.params.Face;

import java.util.Map;

public class EmojiFace {
    private Emotion emotion = new Thinking();
    private Integer confidence = null;
    private Bitmap faceImage;
    private boolean marked;
    private ImageProcessor imgProcessor = new ImageProcessor();
    private Face face;
    private RectF faceRect;
    private TensorFlow tensorFlow;


    EmojiFace(Face face) {
        this.face = face;
        this.faceRect = transformRect(calculateFaceRange(face.getBounds()));
        this.marked = true;
    }

    public void setTensorFlow(TensorFlow tensorFlow) {
        this.tensorFlow = tensorFlow;
    }

    /**
     * updates the faceImage from the current frame
     * @param frame
     */
    public void updateImage(Bitmap frame) {
        /**
         * Rotation und Skalierung
         */
        Bitmap transformedBitmap = imgProcessor.transform(frame);

        faceImage = imgProcessor.cropImage(transformedBitmap, faceRect);

    }

    /**
     * Emotion Recognition
     * sets emotion and confidence of this face
     */
    public void predictEmotion() {

        Map emotionResult = tensorFlow.predictEmotion(faceImage);
        if (emotionResult != null) {
            int emotionNumber = (int)emotionResult.get("emotion");
            switch (emotionNumber) {
                case 0:
                    setEmotion(new Angry());
                    break;
                case 1:
                    setEmotion(new Disgusted());
                    break;
                case 2:
                    setEmotion(new Scared());
                    break;
                case 3:
                    setEmotion(new Happy());
                    break;
                case 4:
                    setEmotion(new Neutral());
                    break;
                case 5:
                    setEmotion(new Sad());
                    break;
                case 6:
                    setEmotion(new Surprised());
                    break;
                default:
                    setEmotion(new Thinking());

            }

            float f = (float) emotionResult.get("confidence");
            int f_neu = (Math.round(f * 100));
            setConfidence(f_neu);

        } else {
            setEmotion(new Thinking());
            setConfidence(null);
        }
    }

    public RectF calculateFaceRange(final Rect face) {
        int viewWidth = 640;
        int viewHeight = 480;
        RectF rectF = FaceUtil.calculateFaceRect(face, viewWidth, viewHeight, 3264,
                2448);
        return rectF;
    }

    private RectF transformRect(RectF rectF) {
        int viewWidth = 640;
        int viewHeight = 480;
        return FaceUtil.transformRect(rectF, viewWidth, viewHeight);
    }

    public void updateFace(Face face) {
        this.face = face;
        this.faceRect = transformRect(calculateFaceRange(face.getBounds()));
    }

    public boolean isMarked() {
        return marked;
    }

    public int emojiXPosition() {
        return (int)faceRect.left;
    }

    public int emojiYPosition() {
        return (int)faceRect.top;
    }

    public int emojiWidth() {
        return (int) faceRect.width();
    }

    public int emojiHeight() {
        return (int) faceRect.height();
    }

    public Emotion getEmotion() {
        return emotion;
    }

    public void setEmotion(Emotion emotion) {
        this.emotion = emotion;
    }

    public Bitmap getFaceImage() {
        return faceImage;
    }

    public Face getFace() {
        return face;
    }

    public void setFace(Face face) {
        this.face = face;
    }

    public void setMarked(boolean marked) {
        this.marked = marked;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }
}
