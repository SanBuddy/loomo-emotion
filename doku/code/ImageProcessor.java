package com.example.android.camera2video;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;

public class ImageProcessor {

    public Bitmap cropImage(Bitmap bitmap, RectF faceRect) {
        int left = (int)(faceRect.left - 0.2 * faceRect.width());
        int top = (int)(faceRect.top - 0.2 * faceRect.height());
        int width = (int) (faceRect.width() * 1.4);
        int height = (int) (faceRect.height() * 1.4);

        return Bitmap.createBitmap(bitmap, left, top, width, height);
    }

    /**
     * Scale and rotate given bitmap
     * @param bitmap
     * @return
     */
    public Bitmap transform(Bitmap bitmap) {

        int viewWidth = 640;
        int viewHeight = 480;

        Matrix matrix = new Matrix();

        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, viewHeight, viewWidth);

        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();

        bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
        matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);

        /**
         * mögliche Skalierung
         */
        /*float scale = Math.max(
                (float) viewHeight / viewHeight,
                (float) viewWidth / viewWidth);

        Log.d("Transform", "scale:" + scale);
        matrix.postScale(scale, scale, centerX, centerY);*/

        matrix.postRotate(-90);

        Bitmap transformedBitmap = Bitmap.createBitmap(bitmap, 0, 0, viewWidth, viewHeight, matrix, true);

        return transformedBitmap;
    }

}
