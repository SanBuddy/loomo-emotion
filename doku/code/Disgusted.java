package com.example.android.camera2video;

public class Disgusted implements Emotion {
    //Image emoji;
    int emoji = R.drawable.disgusted;

    double emojiRatio = 1.09;

    @Override
    public int getEmoji() {
        return emoji;
    }

    public double getEmojiRatio() {
        return emojiRatio;
    }
}
