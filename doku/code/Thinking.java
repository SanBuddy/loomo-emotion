package com.example.android.camera2video;

public class Thinking implements Emotion {
    //Image emoji;
    int emoji = R.drawable.thinking;

    double emojiRatio = 1.09;

    @Override
    public int getEmoji() {
        return emoji;
    }

    public double getEmojiRatio() {
        return emojiRatio;
    }
}
