\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Aufgabenstellung}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Ziel}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Anforderungen}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Teilaufgaben: Emotionserkennung}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Analyse}{3}{section.2}
\contentsline {section}{\numberline {3}Konzept}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Erster Entwurf}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Finaler Entwurf}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}GUI}{5}{subsection.3.3}
\contentsline {section}{\numberline {4}Implementierung}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Architektur}{8}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Camera2VideoFragment}{8}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}FaceRepository}{9}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}EmojiFace}{9}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}ImageProcessor}{10}{subsubsection.4.1.4}
\contentsline {subsubsection}{\numberline {4.1.5}TensorFlow}{10}{subsubsection.4.1.5}
\contentsline {subsubsection}{\numberline {4.1.6}Emotion}{10}{subsubsection.4.1.6}
\contentsline {subsubsection}{\numberline {4.1.7}AutoFitDrawableView}{10}{subsubsection.4.1.7}
\contentsline {subsection}{\numberline {4.2}Verworfene Technologien}{11}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}OpenCV}{12}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Python f\IeC {\"u}r Android}{12}{subsubsection.4.2.2}
\contentsline {section}{\numberline {5}Nutzung von TensorFlow in der App}{12}{section.5}
\contentsline {section}{\numberline {6}Probleme}{12}{section.6}
\contentsline {subsection}{\numberline {6.1}Workflow}{12}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Weiterhin auftretende Probleme}{12}{subsection.6.2}
\contentsline {section}{\numberline {7}Benutzung}{13}{section.7}
\contentsline {subsection}{\numberline {7.1}Android Studio}{13}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Erstellung des Modells mittels TensorFlow}{13}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Loomo}{13}{subsection.7.3}
\contentsline {section}{\numberline {8}Ausblick}{13}{section.8}
\contentsline {section}{\nonumberline Literatur}{15}{section*.2}
