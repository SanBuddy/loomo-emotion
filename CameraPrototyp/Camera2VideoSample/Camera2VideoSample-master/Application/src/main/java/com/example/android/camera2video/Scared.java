package com.example.android.camera2video;

public class Scared implements Emotion {
    //Image emoji;
    int emoji = R.drawable.scared;

    double emojiRatio = 1.09;

    @Override
    public int getEmoji() {
        return emoji;
    }

    public double getEmojiRatio() {
        return emojiRatio;
    }
}
