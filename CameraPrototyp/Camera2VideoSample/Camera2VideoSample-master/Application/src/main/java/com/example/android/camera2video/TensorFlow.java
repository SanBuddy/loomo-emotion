package com.example.android.camera2video;

import android.content.Context;
import android.graphics.Bitmap;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.HashMap;
import java.util.Map;

public class TensorFlow {

    //Variables for TF Processing
    float[] PREDICTIONS = new float[1000];
    private float[] floatValues;
    private TensorFlowInferenceInterface tf;
    private static final String INPUT_NAME = "input"; // remove _1 when using the retrained_graph
    private static final String OUTPUT_NAME = "final_result"; //"output_1"; //"final_result";

    private static final String MODEL_FILE = "file:///android_asset/rounded_graph_500.pb";

    /**
     * code by https://github.com/johnolafenwa/Pytorch-Keras-ToAndroid
     */
    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public Map predictEmotion(Bitmap bitmap){
        tf = new TensorFlowInferenceInterface(context.getAssets(), MODEL_FILE);

        // Resize the image to 224x224
        Bitmap resizedImage = ImageUtils.processBitmap(bitmap, 224);
        // normalize the pixels
        floatValues = ImageUtils.normalizeBitmap(resizedImage, 224, 127.5f, 1.0f);

        // Pass input into tf
        tf.feed(INPUT_NAME, floatValues, 1,224,224,3);

        // compute the predictions
        tf.run(new String[]{OUTPUT_NAME});

        // cipy the output into the PREDICTIONS array
        tf.fetch(OUTPUT_NAME, PREDICTIONS);

        // Obtained highest prediction
        Object[] results = argmax(PREDICTIONS);
        //resultView.findViewById(R.id.testResults);
        int class_index = (Integer) results[0];
        float confidence = (Float) results[1];

        if (confidence > 0.5){
            Map map = new HashMap<>();
            map.put("emotion", class_index);
            map.put("confidence", confidence);
            return map;
            //return class_index;
        }
        else {
            return null;
            //return -1;
        }
    }


    public Object[] argmax(float[] array){

        int best = -1;
        float best_confidence = 0.0f;

        for (int i = 0; i < array.length; i++){

            float value = array[i];

            if(value > best_confidence){
                best_confidence = value;
                best = i;
            }
        }
        return new Object[]{best, best_confidence};
    }
}
